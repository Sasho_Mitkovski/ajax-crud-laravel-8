@extends('layouts.app')
@section('content')

    <!-- AddStudentModal -->
    <div class="modal fade" id="AddStudentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Student</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">

                    <ul id="saveform_errList"></ul>

                    <div class="form group mb-3">
                        <label for="">Name</label>
                        <input type="text" class="name form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Email</label>
                        <input type="text" class="email form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Phone</label>
                        <input type="text" class="phone form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Course</label>
                        <input type="text" class="course form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary add_student">Save</button>
                </div>
            </div>
        </div>
    </div>
    <!--End AddStudentModal -->

    {{-- EditStudentModal --}}
    <div class="modal fade" id="EditStudentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Edit & Update Student</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <ul id="updateform_errList"></ul>
                          <input type="hidden" id="edit_student_id">
                    <div class="form group mb-3">
                        <label for="">Name</label>
                        {{-- Na site inputi stavame id, za da moze da gi zemame starite vrednosti za editiranje --}}
                        <input type="text" id="edit_name" class="name form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Email</label>
                        <input type="text" id="edit_email"  class="email form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Phone</label>
                        <input type="text" id="edit_phone"  class="phone form-control">
                    </div>
                    <div class="form group mb-3">
                        <label for="">Course</label>
                        <input type="text" id="edit_course"  class="course form-control">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary update_student">Update</button>
                </div>
            </div>
        </div>
    </div>
    {{-- End EditStudentModal --}}

   {{-- DeleteStudentModal --}}
   <div class="modal fade" id="DeleteStudentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Delete Student</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                      <input type="hidden" id="delete_student_id">
                      <h4>Are you sure delete student ? </h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                <button type="button" class="btn btn-danger delete_student_btn">Yes, Delete</button>
            </div>
        </div>
    </div>
</div>
{{-- End DeleteStudentModal --}}

    <div class="container py-5">
        <div class="row">
            <div class="col-md-12">
                <div id="success_message"></div>
                <div class="card">
                    <div class="card-header">
                        <h4>Students Data
                            <a href="#" data-bs-toggle="modal" data-bs-target="#AddStudentModal"
                                class="btn btn-primary float-end btn-sm">Add Student</a>
                        </h4>
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Phone</th>
                                    <th>Course </th>
                                    <th>Edit </th>
                                    <th>Delete </th>
                                </tr>
                            </thead>
                            <tbody>
                                {{-- Printanje na studenti so jQuery --}}
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    <script>
        $(document).ready(function() {
            // ============================================== PRINTANJE ============================================================
            // Funkcija za printanje na student vo tabela fetchstudent
            fetchstudent();

            function fetchstudent() {
                $.ajax({
                    type: "GET",
                    url: "/fetch-students",
                    dataType: "json",
                    success: function(response) {
                        // console.log(response.students)
                        $('tbody').html("") //ja prazni tabelata
                        $.each(response.students, function(key, student) {
                            // Ja polni povtorno
                            $('tbody').append(
                                ` <tr>
                                        <td>${student.id}</td>
                                        <td>${student.name}</td>
                                        <td>${student.email}</td>
                                        <td>${student.phone}</td>
                                        <td>${student.course}</td>
                                        <td><button type="button" value="${student.id}" class="edit_student btn btn-success btn-sm">Edit</button></td>
                                        <td><button type="button" value="${student.id}" class="delete_student btn btn-danger btn-sm">Delete</button></td>
                                    </tr>
                                  `
                            )
                        });
                    }
                });
            }

            //===================================== DELETE ==============================================================
            $(document).on('click', '.delete_student',function (e) {
                e.preventDefault()
                var student_id = $(this).val()
                // alert(student_id)
              //  pokazuvanje na modalot za brisenje
                $('#delete_student_id').val(student_id)
                $('#DeleteStudentModal').modal('show')
            });

            $('.delete_student_btn').on('click', function (e) {
                e.preventDefault()

                $(this).text("Deleting")
                var student_id = $('#delete_student_id').val()// zemanje na id od kliknatiot student

                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "DELETE",
                    url: "/delete-student/"+student_id,
                    success: function (response) {
                        console.log(response)
                        $('#success_message').addClass('alert alert-success')
                        $('#success_message').text(response.message);//Printanje poraka od Controller-destroy
                        $('#DeleteStudentModal').modal('hide')
                        $('.delete_student_btn').text("Yes, delete")
                        fetchstudent();
                    }
                });
            });
            //==================================== END DELETE ===========================================================
            // End funkcija za printanje vo tabela
            // ================================================ EDIT student =================================================
            $(document).on('click','.edit_student', function (e) {
                e.preventDefault();
                var student_id = $(this).val()// zemanje na id na kliknatiot student
                // console.log(student_id)
                $('#EditStudentModal').modal('show')
                $.ajax({
                    type: "GET",
                    url: "/edit-student/"+student_id,
                    success: function (response) {
                        // console.log(response)
                        if(response.status == 404){
                            $('#status_message').html("")//prazna poraka
                            $('#status_message').addClass('alert alert-danger')// dodavanje klasa na div-ot za message
                            $('#status_message').text(response.message)
                        }else{
                            $('#edit_name').val(response.student.name)
                            $('#edit_email').val(response.student.email)
                            $('#edit_phone').val(response.student.phone)
                            $('#edit_course').val(response.student.course)
                            $('#edit_student_id').val(student_id)
                        }
                    }
                });
            });
            //================================================ UPDATE =========================================================
            $(document).on('click', '.update_student', function (e) {
                e.preventDefault()

                $(this).text("Updating")
                var student_id = $('#edit_student_id').val()
                var data = {
                    'name' : $('#edit_name').val(),
                    'email' : $('#edit_email').val(),
                    'phone' : $('#edit_phone').val(),
                    'course' : $('#edit_course').val(),
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "PUT",
                    url: "/update-student/"+student_id,
                    data: data,
                    dataType: "json",
                    success: function (response) {
                        if(response.status == 400){
                            $('#updateform_errList').html("");
                            $('#updateform_errList').addClass('alert alert-danger');
                            $.each(response.errors, function(key, err_values) {
                                $('#updateform_errList').append('<li>' + err_values + '</li>');
                            });
                            $('.update_student').text("Update")
                        }else if(response.status == 404){
                            $('#updateform_errList').html("");
                            $('#success_message').addClass('alert alert-success')
                            $('#success_message').text(response.message)
                            $('.update_student').text("Update")
                        }else{
                            $('#updateform_errList').html("");
                            $('#success_message').html("");
                            $('#success_message').addClass('alert alert-success')
                            $('#success_message').text(response.message)

                            $('#EditStudentModal').modal('hide')
                            $('.update_student').text("Update")
                            fetchstudent(); // Funkcija za printanje vo tabela
                        }
                    }
                });
            });
            // ================================================= STORE ======================================================================
            // Kreiranje i dodavanje na nov student
            $(document).on('click', '.add_student', function(e) {
                e.preventDefault();

                var data = {
                    'name': $('.name').val(),
                    'email': $('.email').val(),
                    'phone': $('.phone').val(),
                    'course': $('.course').val(),
                }
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    type: "POST",
                    url: "/students",
                    data: data,
                    dataType: "json",
                    success: function(response) {
                        // console.log(response)
                        // Printanje na error
                        if (response.status == 400) {
                            $('#saveform_errList').html("");
                            $('#saveform_errList').addClass('alert alert-danger');
                            $.each(response.errors, function(key, err_values) {
                                $('#saveform_errList').append('<li>' + err_values + '</li>');
                            })
                        } else {
                            $('#saveform_errList').html("");
                            $('#success_message').addClass('alert alert-success')
                            $('#success_message').text(response.message)
                            $('#AddStudentModal').modal('hide')
                            $('#AddStudentModal').find('input').val("")
                            // Funkcija za printanje na student vo tabela,koga dodademe nov student, vednas da go isprinta vo tabela
                            fetchstudent();
                        }
                    }
                })
            })
        })
    </script>
@endsection
